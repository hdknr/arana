# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import (
    ugettext_lazy as _, 
    ugettext as __,
)

from arana.crawlers import Crawler
import json

# Create your models here.

class Case(models.Model):
    title = models.CharField(
        _('Use Case Title'),
        max_length=50,
    )

    class Meta:
        verbose_name = _('Use Case')
        verbose_name_plural = _('Use Cases')

    def __unicode__(self):
        return self.title or ''


class Action(models.Model):

    ACTION_GET = 0
    ACTION_POSTBACK = 1
    ACTION_POST = 2

    case = models.ForeignKey(Case) 
    ordinal = models.IntegerField(default=0)
    title = models.CharField(
        _('Action Title'),
        max_length=50, null=True, blank=True, default='',
    )
    action = models.IntegerField(
        _('Action Type'), default=ACTION_GET,
        choices=(
            (ACTION_GET, _('Action Get')),
            (ACTION_POST, _('Action Post')),
            (ACTION_POSTBACK, _('Action Post Back')),
        ),
    )
    uri = models.CharField(
        _('URI'), max_length=300,)

    form_selector = models.CharField( 
        _('Form Selector'), max_length=50,
        null=True, blank=True, default='',
    )

    form_params =  models.TextField(
        _('Form Parameters'),
        null=True, blank=True, default='',
    ) 

    class Meta:
        verbose_name = _('Action')
        verbose_name_plural = _('Actions')
        ordering = ['-case', 'ordinal', ]

    def __unicode__(self):
        return self.title or ''

    def perform(self, crawler):
        if self.action == Action.ACTION_GET:
            crawler.get(self.uri) 
        elif self.action == Action.ACTION_POST:
            crawler.post(self.uri, json.loads(self.form_params))
        else:
            crawler.post_back(
                self.uri, 
                self.form_selector,
                json.loads(self.form_params))


class Run(models.Model):
    case = models.ForeignKey(Case) 
    title = models.CharField(
        _('Run Title'),
        max_length=50,
    )
    
    class Meta:
        verbose_name = _('Run')
        verbose_name_plural = _('Run')

    def __unicode__(self):
        return self.title or ''

    def perform(self):
        self.result_set.all().delete()
        crawler = Crawler()
        for action in self.case.action_set.all():
            result, created = Result.objects.get_or_create(
                run=self,
                action=action,
                ordinal=action.ordinal,
            )
            result.perform(crawler)


class Result(models.Model):
    run = models.ForeignKey(Run) 
    action = models.ForeignKey(Action) 
    ordinal = models.IntegerField(default=0)
    status_code = models.IntegerField(default=0)
    headers = models.TextField(
        null=True, blank=True, default=None)
    html = models.TextField(
        null=True, blank=True, default=None)
    errors = models.TextField(
        null=True, blank=True, default=None)

    class Meta:
        verbose_name = _('Result')
        verbose_name_plural = _('Result')
        ordering = ['-run', 'ordinal', ]

    def __unicode__(self):
        return self.action and self.action.__unicode__() or ''

    def perform(self, crawler):
        self.action.perform(crawler)
        self.status_code = crawler.res.status_code
        self.headers = crawler.serialized_headers
        self.html = crawler.html
        self.errors = crawler.errors
        self.save()
