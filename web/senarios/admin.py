# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from django.db.models import get_app, get_models
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse
from django.utils.translation import (
    ugettext_lazy as _, 
    ugettext as __,
)

from models import (
    Case, Action,
    Run, Result,
)

_excludes = [
]

### Action

class ActionAdminForm(forms.ModelForm):

    class Meta:
        model = Action


class ActionAdmin(admin.ModelAdmin):

    list_display =  [f.name for f in Action._meta.fields]
    form = ActionAdminForm


class ActionAdminInline(admin.StackedInline):
    model = Action
    form = ActionAdminForm
    extra = 0

### Case


class CaseAdminForm(forms.ModelForm):

    class Meta:
        model = Case


class CaseAdmin(admin.ModelAdmin):

    list_display =  [f.name for f in Case._meta.fields]
    inlines = [ActionAdminInline, ]
    form = CaseAdminForm

### Result

class ResultAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ResultAdminForm, self).__init__(*args, **kwargs)
        qs = self.fields['action'].queryset
        if self.instance:
            try:
                self.fields['action'].queryset = qs.filter(
                    case=self.instance.run.case)
            except Exception, ex:
                print ex
                pass

    class Meta:
        model = Result


class ResultAdmin(admin.ModelAdmin):

    list_display =  [f.name for f in Result._meta.fields]
    form = ResultAdminForm


class ResultAdminInline(admin.StackedInline):
    model = Result
    form = ResultAdminForm
    extra = 0

### Run

class RunAdminForm(forms.ModelForm):

    class Meta:
        model = Run


def perform_run(modeladmin, request, queryset):
    for run in queryset:
        run.perform()

perform_run.short_description = _(u"Perform selected Runs")


class RunAdmin(admin.ModelAdmin):

    list_display =  [f.name for f in Run._meta.fields]
    inlines = [ResultAdminInline, ]
    form = RunAdminForm
    actions = [perform_run, ]


### main

for model in get_models(get_app(__name__.split('.')[-2:][0])):
    if model.__name__ in _excludes:
        continue

    name = "%sAdmin" % model.__name__

    admin_class = globals().get(name, None)
    if admin_class is None:
        params = dict(
            list_display=tuple([f.name for f in model._meta.fields]),)
        admin_class = type(
            "%sAdmin" % model.__name__,
            (admin.ModelAdmin,),
            params,
        )

    admin.site.register(model, admin_class)

