# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('senarios', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ordinal', models.IntegerField(default=0)),
                ('status_code', models.IntegerField(default=0)),
                ('headers', models.TextField(default=None, null=True, blank=True)),
                ('html', models.TextField(default=None, null=True, blank=True)),
                ('action', models.ForeignKey(to='senarios.Action')),
            ],
            options={
                'ordering': ['-run', 'ordinal'],
                'verbose_name': 'Result',
                'verbose_name_plural': 'Result',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Run',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name='Run Title')),
                ('case', models.ForeignKey(to='senarios.Case')),
            ],
            options={
                'verbose_name': 'Run',
                'verbose_name_plural': 'Run',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='result',
            name='run',
            field=models.ForeignKey(to='senarios.Run'),
            preserve_default=True,
        ),
        migrations.AlterModelOptions(
            name='action',
            options={'ordering': ['-case', 'ordinal'], 'verbose_name': 'Action', 'verbose_name_plural': 'Actions'},
        ),
    ]
