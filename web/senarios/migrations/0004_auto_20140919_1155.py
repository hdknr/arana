# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('senarios', '0003_auto_20140919_1136'),
    ]

    operations = [
        migrations.RenameField(
            model_name='action',
            old_name='from_params',
            new_name='form_params',
        ),
    ]
