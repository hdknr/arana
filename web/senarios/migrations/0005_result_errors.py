# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('senarios', '0004_auto_20140919_1155'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='errors',
            field=models.TextField(default=None, null=True, blank=True),
            preserve_default=True,
        ),
    ]
