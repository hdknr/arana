# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('senarios', '0002_auto_20140919_1103'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='action',
            field=models.IntegerField(default=0, verbose_name='Action Type', choices=[(0, 'Action Get'), (2, 'Action Post'), (1, 'Action Post Back')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='action',
            name='form_selector',
            field=models.CharField(default=b'', max_length=50, null=True, verbose_name='Form Selector', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='action',
            name='from_params',
            field=models.TextField(default=b'', null=True, verbose_name='Form Parameters', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='action',
            name='uri',
            field=models.CharField(default='http://localhost', max_length=300, verbose_name='URI'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='action',
            name='title',
            field=models.CharField(default=b'', max_length=50, null=True, verbose_name='Action Title', blank=True),
        ),
    ]
