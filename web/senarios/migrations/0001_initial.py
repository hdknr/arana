# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ordinal', models.IntegerField(default=0)),
                ('title', models.CharField(max_length=50, verbose_name='Action Title')),
            ],
            options={
                'ordering': ['-case', '-ordinal'],
                'verbose_name': 'Use Case',
                'verbose_name_plural': 'Use Cases',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Case',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name='Use Case Title')),
            ],
            options={
                'verbose_name': 'Use Case',
                'verbose_name_plural': 'Use Cases',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='action',
            name='case',
            field=models.ForeignKey(to='senarios.Case'),
            preserve_default=True,
        ),
    ]
