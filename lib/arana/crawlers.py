from urlparse import urljoin
import hashlib
import requests
from bs4 import BeautifulSoup as Soup
import traceback
import json
from werkzeug.datastructures import MultiDict


class Crawler(object):
    def __init__(self):
        self.req = requests.session()
        self.res = None
        self.last_uri = ""
        self.errors = ""

    def get(self, uri, params=None):
        self.res = self.req.get(
            uri, 
            params=None,
            verify=False, allow_redirects=False)
        self.last_uri = uri

    def post(self, uri, data):
        if isinstance(data, MultiDict):
            data = dict(data.lists())

        self.res = self.req.post(
            uri, data=data, 
            verify=False, allow_redirects=False)
        self.last_uri = uri

    @property
    def is_html(self):
        return all([
            self.res,
            self.res.headers["content-type"].startswith("text/html")
        ])
        
    @property
    def soup(self):
        return self.is_html and Soup(self.res.content) or Soup()

    def form_data(self, form_node):

        data = MultiDict(list(
           (t['name'], t.get('value', '')) 
           for i in ['text', 'hidden', 'password', ]
           for t in form_node.select('input[type=%s]' % i)
        ) + list(
           (t['name'], t.text)
           for t in form_node.select('textarea')
        ) + list(
           (t['name'], t.get('value', '')) 
           for t in form_node.select('input[checked]')
        ) + list(
            (s.parent['name'], s.get('value', s.text))
            for s in form_node.select('option[selected]')
        ))

        # TODO: "file", "image", 
        # TODO: HTML5 -- input(list) and datalist

        return data

    def post_back(self, uri, selector, params, index=0):
        if uri != self.last_uri:
            self.get(uri)

        try:
            form = self.soup.select(selector)[index]
        except:
            self.errors = traceback.format_exc()
            return
         
        vals = self.form_data(form)
        vals.update(params)

        uri = urljoin(uri, form.get('action', ''))

        if form['method'] == 'post':
            self.post(uri, vals)
        else:
            self.get(uri, params=vals)

        self.last_uri = uri

    @property
    def serialized_headers(self):
        data = {}
        data.update(self.res.headers)
        return json.dumps(data, indent=2)

    @property
    def html(self):
        if self.is_html:
            return Soup(self.res.content).prettify()
        return None

