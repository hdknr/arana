from django.template.response import TemplateResponse
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from bs4 import BeautifulSoup as Soup

from forms import Profile, Interest

def default(request):
    template_name = 'arana/default.html'
    return TemplateResponse(
        request, template_name,
        dict())


def tests_postback(request):
    template_name = 'arana/tests/postback.html'
    data = request.POST or None
    form = Profile(data=data)
    if request.method == "POST":
        if form.is_valid():
            if form.cleaned_data['confirm']: 
                return HttpResponseRedirect(
                    reverse('arana_default'))
    
    return TemplateResponse(
        request, template_name,
        dict(form=form,))
