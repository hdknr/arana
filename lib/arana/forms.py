# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import (
    ugettext_lazy as _, 
    ugettext as __,
)


class Interest:
    MUSIC = 1    
    SPORTS = 2
    COOKING = 3
    Choices = (
        (MUSIC, __('Music')),
        (SPORTS, __('Sports')),
        (COOKING, __('Cooking')),
    )

 
class Profile(forms.Form):
    interest = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        choices=Interest.Choices,
        required=True, 
        label=_('Interest')
    )
    confirm = forms.BooleanField(
        required=True, 
    )
