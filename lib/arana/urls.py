# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url 
from views import *


urlpatterns = patterns('',
    url(r'tests/postback', tests_postback ,name="arana_tests_postback",),
    url(r'', default ,name="arana_default",),
)
